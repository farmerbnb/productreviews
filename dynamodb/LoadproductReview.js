
                    
/**
 * Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * This file is licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License. A copy of
 * the License is located at
 *
 * http://aws.amazon.com/apache2.0/
 *
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
*/
var AWS = require("aws-sdk");
var fs = require('fs');

AWS.config.update({
    region: "ap-southeast-1",
    endpoint: "https://dynamodb.ap-southeast-1.amazonaws.com"
});

var docClient = new AWS.DynamoDB.DocumentClient();

console.log("Importing review into DynamoDB. Please wait.");

var allReviews = JSON.parse(fs.readFileSync('productReview.json', 'utf8'));
allReviews.forEach(function(review) {
    var params = {
        TableName: "ProductReviews",
        Item: {
            "reviewId": review.reviewId,
            "productId": review.productId,
			"buyerId":  review.buyerId,
			"reviewMsg": review.reviewMsg,
			"rating": review.rating
        }
    };

    docClient.put(params, function(err, data) {
       if (err) {
           console.error("Unable to add review buyerId", review.buyerId, ". Error JSON:", JSON.stringify(err, null, 2));
       } else {
           console.log("PutItem succeeded buyerId :", review.buyerId);
       }
    });
});

                