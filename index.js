const AWS = require('aws-sdk');
AWS.config.update({ region: "ap-southeast-1"});
const response = {
  statusCode: 200,
  body: JSON.stringify('Hello from Lambda!'),
};

const documentClient = new AWS.DynamoDB.DocumentClient({ region: "ap-southeast-1"});
const {getProductReviews,addProductReviews} = require("./productReviews.js");
exports.handler = async (event, context) => {
  try {
    
    const qsq = event.queryStringParameters;
  
    let res = null;
    if(qsq){
      let productId = qsq.productId;
      productId = 123;// test
      let buyerId = qsq.productId;
      if(buyerId || productId){
        res =  await getProductReviews(buyerId,productId,documentClient);
      }
	  
	  
      
    }
    else{
     // post paymentMethod
      const reviewId = event.reviewId;
      if(reviewId){
        await addProductReviews(reviewId,event,documentClient);
      }
      
    }
    
    
    if(res){
        response.body = res;
    }
  } catch (err) {
    console.log(err);
  }
  return response;
}