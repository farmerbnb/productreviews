async function getProductReviews(buyerId,productId,dbconnect){
	
	let result = null;
	if(buyerId||productId){
		var params = {};
		try {
			
		  params = {
				TableName: 'ProductReviews',
				FilterExpression:'productId=:productId',
				ExpressionAttributeValues: {
			    ':productId':productId
				}	
				}  
			 /*if(buyerId&&productId){
				 params = {
				TableName: 'ProductReviews',
				FilterExpression:'buyerId=:buyerId AND productId=:productId',
				ExpressionAttributeValues: {
			    ':buyerId': buyerId,
			    ':productId': productId
				}
				}
			}
			else if(buyerId)
			{
				 params = {
				TableName: 'ProductReviews',
				FilterExpression:'buyerId=:buyerId',
				ExpressionAttributeValues: {
			    ':buyerId': buyerId
				}	
				}
			}
			else
			{
				 params = {
				TableName: 'ProductReviews',
				FilterExpression:'productId=:productId',
				ExpressionAttributeValues: {
			    ':productId':productId
				}	
				}
			}*/
			
		
			// Call DynamoDB to read the item from the table
		  const data = await dbconnect.scan(params).promise();
          if(data){
              result = JSON.stringify(data.Items);
          }    		
			
		
			 
	  } catch (ex) {
		console.log("Get calling error: "+ex);
	  }
		
	}
	else{
		throw Error('missing Ids');
	}
	return result;
}

async function addProductReviews(reviewId,json,dbconnect){

//	let result = null;
	if(reviewId){
		try {
		    var params = {
			TableName: 'ProductReviews',
			Item: {
				"reviewId":reviewId,
				"buyerId":json.productId,
				"productId":json.productId,
				"reviewMsg": json.reviewMsg,
				"rating": json.rating
			  }
			};
			// Call DynamoDB to read the item from the table
		   await dbconnect.put(params).promise();
            		
			 
	  } catch (ex) {
		console.log("post calling error: "+ex);
	  }
		
	}
	else{
		throw Error('missing reviewId');
	}
//	return result;
}

module.exports = {getProductReviews,addProductReviews};
