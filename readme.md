### Get product review by productId / buyerId 
url :https://kbo2sjozel.execute-api.ap-southeast-1.amazonaws.com/dev/productreviews?productId=123

response body:

[
  {
    "reviewMsg": "good quality",
    "rating": 5,
    "buyerId": "lijndhyeju3jo83kydff=",
    "reviewId": "1598794766kdmh=",
    "productId": 123
  },
  {
    "reviewMsg": "highly recommended!",
    "rating": 4,
    "buyerId": "dklsumnh23ji9ssfv=",
    "reviewId": "158368434kdj=",
    "productId": 123
  }
]

### POST product review 

request body:

{
  "reviewId": "158663498jhydk=",
  "buyerId": "lijnhjsngju3jo83kydff=",
  "productId": 234,
  "reviewMsg": "good quality",
  "rating": 5
}
